Para jogar é necessário compilar e executar o jogo:

# PARA COMPILAR O JOGO
->Em um sistema Linux, no terminal, acesse a pasta denominada EP1, digite o comando "make" e aguarde a compilação, uma vez finalizada, execute o jogo.

#PARA EXECUTAR O JOGO
->Para executar o jogo, digite o comando "bin/jogo" e ele será executado.
