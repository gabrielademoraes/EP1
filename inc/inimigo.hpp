#ifndef INIMIGO_HPP
#define INIMIGO_HPP

#include <iostream>
#include <string>
#include "gameObject.hpp"
#include "mapa.hpp"


class Inimigo : public GameObject {
	private:
		int dir;
	public:
		Inimigo(Mapa *mapa);
		Inimigo(char caracter, int posicao_x, int posicao_y);

		void executar();


};

#endif