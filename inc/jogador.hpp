#ifndef JOGADOR_HPP
#define JOGADOR_HPP

#include <iostream>
#include <string>
#include "gameObject.hpp"


class Jogador : public GameObject {
	private:
	
	public:
		Jogador();
		Jogador(char caracter, int posicao_x, int posicao_y);
		
		void mostrar();

		void movimento();


};

#endif