#ifndef MAPA_HPP
#define MAPA_HPP

#include <iostream>
#include <string>
#include <vector>
#include "gameObject.hpp"
#include "moeda.hpp"
#include "inimigo.hpp"
#include "saida.hpp"

#define LINHA 20
#define COLUNA 50

using namespace std;

class Mapa{

	private:      
		char tamanho[COLUNA][LINHA];
		vector<GameObject*> *listaObjetos;
		
	public:		
		Mapa(vector<GameObject*> *listaObjetos);

		void carregar(string arq);
		void mostrar();
		bool colisao(int x, int y);
		vector<GameObject*>* getGameObjects();


		void setTamanhoMapa(); 
		void getTamanhoMapa();
		void adicionaJogador(char caracter, int posicao_x, int posicao_y);
};

#endif
