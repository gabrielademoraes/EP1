#ifndef GAMEOBJECT_HPP
#define GAMEOBJECT_HPP
#include <ncurses.h>

class Mapa;

class GameObject{
	protected:
		int posicao_x;
		int posicao_y;
		char caracter;
		bool ativo;
		Mapa *mapa;

	public:
		GameObject();		
		GameObject(char caracter, int posicao_x, int posicao_y);

		virtual void setMapa(Mapa *mapa);
		virtual int getPosicao_x();
		virtual void setPosicao_x(int posicao_x);

		virtual int getPosicao_y();
		virtual void setPosicao_y(int posicao_y);

		virtual char getCaracter();
		virtual void setCaracter(char caracter);
		virtual bool colidiu(int x, int y);

		virtual void executar();

		virtual void mostrar();
		virtual void setAtivo(bool ativo);
		virtual bool getAtivo();

};
#endif