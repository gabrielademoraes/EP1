#ifndef MOTOR_HPP
#define MOTOR_HPP

#include <iostream>
#include <string>
#include <vector>
#include "mapa.hpp"
#include "motor.hpp"
#include "jogador.hpp"
#include "gameObject.hpp"

#define LINHA 20
#define COLUNA 50

using namespace std;

class Motor{

	private:		
		Mapa *mapa;
		Jogador *jogador;
		int getUltimaTecla();
		int pontos;
		vector<GameObject*> listaObjetos;
	public:		
		Motor();
		void setMapa(string arq);
		int executar();	
		int getPontos();
		void setPontos(int pontos);
		void carregarJogo();	
};

#endif
