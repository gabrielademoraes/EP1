#include <iostream>
#include <ncurses.h>
#include <string>
#include <stdlib.h>
#include <unistd.h>
#include <stdio_ext.h>
#include "mapa.hpp"
#include "jogador.hpp"
#include "motor.hpp"

using namespace std;



int main(int argc, char ** argv){
	char estado = 'm', c;
	int estadoMotor;
	initscr();
	cbreak();
	noecho();
	scrollok(stdscr, TRUE);
	nodelay(stdscr, TRUE);
	curs_set(0);

	
	Motor *motor = new Motor();
	motor->setMapa("mapa.txt");

	while(TRUE){
		switch(estado){
			case 'm':
				move(0,0);
				printw("S - Iniciar jogo\n");
				printw("H - Ajuda\n");
				printw("C - Créditos\n");
				
				refresh();
				c = getch();
				switch(c){
					case 's':
						motor->carregarJogo();
						estado = 'e';

					break;

					case 'h':
						clear();
						printw("\n\n\n\n\nPersonagens");
						printw("\n< - Jogador");
						printw("\n8 - Saída");
						printw("\n! - Inimigos");
						printw("\n* - Pontos");
						printw("\n\nComandos");
						printw("\nSeta para cima - move o jogador para cima");
						printw("\nSeta para baixo - move o jogador para baixo");
						printw("\nSeta para direita - move o jogador para direita");
						printw("\nSeta para esquerda - move o jogador para esquerda");
						printw("\n\nObjetivo: chegar na saída");
						printw("\n\n\nPara sair digite ctrl+c");
						printw("\n\n\nDigite um caracter do menu:");

					break;

					case 'c':
						printw("\n\nCréditos");
						printw("\n\nGabriela Chaves de Moraes - 16/0006872");
						printw("\n\n\nDigite um caracter do menu:");
					break;
				}
			break;

			case 'e':
				estadoMotor = motor->executar();
				switch(estadoMotor){
					case -1:
						clear();
						move(10,0);
						printw("Morreu!");
						estado = 'm';
					break;
					case 0:
						clear();
						move(10,0);
						printw("Vitoria!");
						printw("Pontos: %d", motor->getPontos());
						estado = 'm';	
					break;
				}

			break;
		}					

		napms(100);		
	}

	endwin();
	return 0;

}
