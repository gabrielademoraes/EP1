#include "mapa.hpp"

#define LINHAS 20
#define COLUNAS 50

using namespace std;

Mapa::Mapa(vector<GameObject*> *listaObjetos){	
	this->listaObjetos = listaObjetos;
}

void Mapa::carregar(string arq){
	ifstream mapa(arq.c_str());

	string aux;
	Moeda *moeda;
	Inimigo *inimigo;
	Saida *saida;
	char c;

	for(int linha = 0; linha < LINHAS; linha++){
		getline(mapa, aux);		
		for(int coluna = 0; coluna < COLUNAS; coluna++){
			c = aux[coluna];
			switch(c){
				case '*':
					moeda = new Moeda('*', coluna, linha);
					moeda->setMapa(this);
					listaObjetos->push_back(moeda);
					c = '.';
				break;
				case '!':
					inimigo = new Inimigo('!', coluna, linha);
					inimigo->setMapa(this);
					listaObjetos->push_back(inimigo);
					c = '.';
				break;
				case '8':
					saida = new Saida('8', coluna, linha);
					saida->setMapa(this);
					listaObjetos->push_back(saida);
					c = '.';

				break;				
			}
			this->tamanho[coluna][linha] = c;
			
		}
	}

	mapa.close();

}

void Mapa::mostrar(){
	move(0, 0);
	for(int linha = 0; linha < LINHAS; linha++){
		for(int coluna = 0; coluna < COLUNAS; coluna++){
			printw("%c", this->tamanho[coluna][linha]);			
		}
		printw("\n");
	}	
}

bool Mapa::colisao(int x, int y){
	return this->tamanho[x][y]!='.';
}


void Mapa::setTamanhoMapa(){
	
}

void Mapa::getTamanhoMapa(){
	for(int coluna = 0; coluna < COLUNAS; coluna++){
		for(int linha = 0; linha < LINHAS; linha++){
			printw("%c", this->tamanho[coluna][linha]);			
		}
		printw("\n");
	}

}

void Mapa::adicionaJogador(char jogador, int posicao_x, int posicao_y){
	this->tamanho[posicao_x][posicao_y] = jogador;
}


vector<GameObject*>* Mapa::getGameObjects(){
	return this->listaObjetos;
}