#include "inimigo.hpp"
#include <iostream>
#include <string>
#include <stdio_ext.h>
#include <ncurses.h>

using namespace std;

Inimigo::Inimigo(Mapa* mapa){}

Inimigo::Inimigo(char caracter, int posicao_x, int posicao_y){
	this->caracter = caracter;
	this->posicao_x = posicao_x;
	this->posicao_y = posicao_y;
	this->dir = 1;
}


void Inimigo::executar(){	
	int x;
	x = this->posicao_x + dir;
	if (mapa->colisao(x, this->posicao_y)){
		dir *= -1;
	}
	else{
		this->posicao_x = x;
	}
}