#include "gameObject.hpp"
#include <iostream>

GameObject::GameObject(){
	ativo = true;
}


GameObject::GameObject(char caracter, int posicao_x, int posicao_y){
	ativo = true;
	this->caracter = caracter;
	this->posicao_x = posicao_x;
	this->posicao_y = posicao_y;
}

int GameObject::getPosicao_x(){
	return posicao_x;
}

void GameObject::setPosicao_x(int posicao_x){
	this->posicao_x = posicao_x;
}

int GameObject::getPosicao_y(){
	return posicao_y;
}

void GameObject::setPosicao_y(int posicao_y){
	this->posicao_y = posicao_y;
}

char GameObject::getCaracter(){
	return caracter;
}

void GameObject::setCaracter(char caracter){
	this->caracter = caracter;
}

bool GameObject::colidiu(int x, int y){
	return ((this->posicao_x==x)&&(this->posicao_y==y));
}

void GameObject::mostrar(){	
	move(this->posicao_y, this->posicao_x);
	printw("%c", this->caracter);
}

void GameObject::setAtivo(bool ativo){
	this->ativo = ativo;
}
bool GameObject::getAtivo(){
	return this->ativo;
}

void GameObject::executar(){

}

void GameObject::setMapa(Mapa *mapa){
	this->mapa = mapa;
}