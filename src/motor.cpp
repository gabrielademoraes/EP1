#include "motor.hpp"
#include <iostream>
#include <fstream>
#include <ncurses.h>
#include <stdio_ext.h>


using namespace std;

Motor::Motor(){
	mapa = new Mapa(&this->listaObjetos);	
	jogador = new Jogador('>', 1, 1);
	pontos = 0;
	
}

int Motor::getUltimaTecla(){
	int c, a;
	do{
		a = c;
		c = getch();			
	} while(c!=-1);
	return a;
}

void Motor::setMapa(string arq){	
	mapa->carregar(arq);
}


int Motor::executar(){	
	int key, x, y;
	GameObject *obj;

	mapa->mostrar();

	key = this->getUltimaTecla();
	x = jogador->getPosicao_x();
	y = jogador->getPosicao_y();

	if (key!=-1){
		switch(key){
			case 65:
				y--;			
			break;
			case 66:
				y++;
			break;
			case 67:
				x++;
			break;
			case 68:
				x--;
			break;
			case 'q':
				return false;
			break;
		}
		if (!mapa->colisao(x, y)){
			jogador->setPosicao_x(x);
			jogador->setPosicao_y(y);
		}		
	}	

	jogador->mostrar();
	for (unsigned int i=0; i < listaObjetos.size();i++){
		obj = listaObjetos[i];
		if (obj->getAtivo()){
			if (obj->colidiu(x, y)){
				switch(obj->getCaracter()){
					case '*':
						pontos++;
						obj->setAtivo(false);
					break;
					case '!':
						move(21, 30);
						return -1;
					break;
					case '8':
						return 0;
					break;
				}				
			}	
			else{								
				obj->mostrar();						
				obj->executar();
			}
		}
	}		

	move(20, 0);
	printw("Pontos: %d", pontos);

	refresh();
	return 1;
}
void Motor::setPontos(int pontos){
	this->pontos = pontos;
}

int Motor::getPontos(){
	return pontos;
}

void Motor::carregarJogo(){
	jogador->setPosicao_x(1);
	jogador->setPosicao_y(1);
	listaObjetos.clear();
	mapa->carregar("mapa.txt");
	this->setPontos(0);
}