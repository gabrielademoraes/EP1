#include "jogador.hpp"
#include <iostream>
#include <string>
#include <stdio_ext.h>
#include <ncurses.h>

using namespace std;

Jogador::Jogador(){}

Jogador::Jogador(char caracter, int posicao_x, int posicao_y){
	this->caracter = caracter;
	this->posicao_x = posicao_x;
	this->posicao_y = posicao_y;
}

void Jogador::mostrar(){
	move(this->posicao_y, this->posicao_x);
	printw("%c", caracter);
}

void Jogador::movimento(){
	int direcao;

	direcao = getch();

	switch(direcao){
		case KEY_UP:
			this->setPosicao_y(1);
		case KEY_DOWN:
			this->setPosicao_y(-1);
		case KEY_RIGHT:
			this->setPosicao_x(1);
		case KEY_LEFT:
			this->setPosicao_x(-1);
	}
}